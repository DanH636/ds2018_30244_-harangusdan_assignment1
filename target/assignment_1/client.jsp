<%--
  Created by IntelliJ IDEA.
  User: haran
  Date: 10/28/2018
  Time: 11:54 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Client Page</title>
    <style type="text/css">
        .paddingBtm {
            padding-bottom: 12px;
        }
    </style>
</head>
<body>
    <h2>Client Page</h2>
    <form id="getAllFlightsForm" name="AllFlightsForm" method="get" action="clientServlet">
        <div id="ViewAll">
            <input type="submit" value="ViewAll" />
        </div>
    </form>

    <form id="getLocalTime" name="LocalTimeForm" action="earthToolsServlet" method="get">
        <div class="paddingBtm">
            <span>Flight number: </span><input type="text" name="getFlightLocalTime" />
        </div>
        <div id="GetLocalTimeButton">
            <input type="submit" value="Get local time" />
        </div>
    </form>
</body>
</html>
