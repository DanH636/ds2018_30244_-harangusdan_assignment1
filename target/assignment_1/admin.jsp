<%--
  Created by IntelliJ IDEA.
  User: haran
  Date: 10/28/2018
  Time: 11:56 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin Page</title>
    <style type="text/css">
        .paddingBtm {
            padding-bottom: 12px;
        }
    </style>
</head>
<body>

<h2>Add flight</h2>
<form id="addFlightFormId" name="AddFlightForm" method="post" action="adminServlet">
    <div class="paddingBtm">
        <span>Flight Number: </span><input type="text" name="flightNumberAdd" />
    </div>
    <div class="paddingBtm">
        <span>Airplane Type: </span><input type="text" name="airplaneTypeAdd" />
    </div>
    <div class="paddingBtm">
        <span>Departure City: </span><input type="text" name="departureCityAdd" />
    </div>
    <div class="paddingBtm">
        <span>Departure Time: </span><input type="text" name="departureTimeAdd" />
    </div>
    <div class="paddingBtm">
        <span>Arrival City: </span><input type="text" name="arrivalCityAdd" />
    </div>
    <div class="paddingBtm">
        <span>Arrival Time: </span><input type="text" name="arrivalTimeAdd" />
    </div>
    <div id="AddBtn">
        <input type="submit" value="Add" />
    </div>
</form>

<h2>Update flight</h2>
<form id="updateFlightFormId" name="addFlightForm" method="post" action="updateServlet">
    <div class="paddingBtm">
        <span>ID: </span><input type="text" name="idUpdate" />
    </div>
    <div class="paddingBtm">
        <span>Flight Number: </span><input type="text" name="flightNumberUpdate" />
    </div>
    <div class="paddingBtm">
        <span>Airplane Type: </span><input type="text" name="airplaneTypeUpdate" />
    </div>
    <div class="paddingBtm">
        <span>Departure City: </span><input type="text" name="departureCityUpdate" />
    </div>
    <div class="paddingBtm">
        <span >Departure Time: </span><input type="text" name="departureTimeUpdate" />
    </div>
    <div class="paddingBtm">
        <span>Arrival City: </span><input type="text" name="arrivalCityUpdate" />
    </div>
    <div class="paddingBtm">
        <span>Arrival Time: </span><input type="text" name="arrivalTimeUpdate" />
    </div>
    <div id="UpdateBtn">
        <input type="submit" value="Update" />
    </div>
</form>

<h2>Get flight</h2>
<form action="adminServlet" name="getFlightForm" method="get">
    <div class="paddingBtm">
        <span>Flight number: </span><input type="text" name="getFlightNumber" />
    </div>
    <div id="ViewButton">
        <input type="submit" value="View" />
    </div>
</form>

<h2>Delete flight</h2>
<form action="deleteServlet" name="deleteFlightForm" method="post">
    <div class="paddingBtm">
        <span>Flight ID: </span><input type="text" name="deleteFlightId" />
    </div>
    <div id="DeleteButton">
        <input type="submit" value="Delete" />
    </div>
</form>
</body>
</html>
