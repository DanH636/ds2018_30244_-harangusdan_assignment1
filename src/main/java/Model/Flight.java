package Model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "flight")
public class Flight {
    @Id
    @Column
    private int id;

    @Column(name = "flight_number")
    private int flightNumber;

    @Column(name = "airplane_type")
    private String airplaneType;

    @ManyToOne (targetEntity = City.class)
    @JoinColumn(name = "departure_city")
    private City departureCity;

    @Column(name = "departure_time")
    private Date departureTime;

    @ManyToOne(targetEntity = City.class)
    @JoinColumn(name = "arrival_city")
    private City arrivalCity;

    @Column(name = "arrival_time")
    private Date arrivalTime;

    public Flight() {
    }

    public Flight(int id, int flightNumber, String airplaneType, City departureCity, Timestamp departureTime, City arrivalCity, Timestamp arrivalTime) {
        this.id = id;
        this.flightNumber = flightNumber;
        this.airplaneType = airplaneType;
        this.departureCity = departureCity;
        this.departureTime = departureTime;
        this.arrivalCity = arrivalCity;
        this.arrivalTime = arrivalTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(int flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(String airplaneType) {
        this.airplaneType = airplaneType;
    }

    public City getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(City departureCity) {
        this.departureCity = departureCity;
    }

    public Date getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }

    public City getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(City arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    public Date getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Date arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    @Override
    public String toString() {
        return "Flight{" +
                "id=" + id +
                ", flightNumber=" + flightNumber +
                ", airplaneType='" + airplaneType + '\'' +
                ", departureCity=" + departureCity +
                ", departureTime=" + departureTime +
                ", arrivalCity=" + arrivalCity +
                ", arrivalTime=" + arrivalTime +
                '}';
    }
}
