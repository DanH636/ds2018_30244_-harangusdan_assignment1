package DAO;

import Model.City;
import Model.Flight;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.*;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public class FlightDAO {
    private static final Log LOGGER = LogFactory.getLog(UserDAO.class);

    private SessionFactory factory;

    public FlightDAO(SessionFactory factory) {
        this.factory = factory;
    }

    public Flight getFlight(int flightNumber) {
        Session session = factory.openSession();
        Transaction transaction = null;
        List<Flight> flights = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("FROM Flight WHERE flight_number = :flightNumber");
            query.setParameter("flightNumber", flightNumber);
            flights = query.list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return flights != null && !flights.isEmpty() ? flights.get(0) : null;
    }

    public List<Flight> getAllFlights() {
        Session session = factory.openSession();
        Transaction transaction = null;
        List<Flight> flights = null;
        try {
            transaction = session.beginTransaction();
            flights = session.createQuery("FROM Flight").list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return flights;
    }

    public Flight addFlight(Flight flight) {
        int flightId = -1;
        Session session = factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            flightId = (Integer) session.save(flight);
            flight.setId(flightId);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
        return flight;
    }

    public void deleteFlight(int id) {
        Session session = factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("delete FROM Flight WHERE id = :id");
            query.setParameter("id", id);
            query.executeUpdate();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
    }

    public void updateFlight(int id, int flightNumber, String airplaneType, City departureCity, Date departureTime, City arrivalCity, Date arrivalTime) {
        Session session = factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("UPDATE Flight SET flight_number = :flightNumber, airplane_type = :airplaneType, departure_city = :departureCity, departure_time = :departureTime, arrival_city = :arrivalCity, arrival_time = :arrivalTime WHERE id = :id");
            query.setParameter("id", id);
            query.setParameter("flightNumber",flightNumber);
            query.setParameter("airplaneType", airplaneType);
            query.setParameter("departureCity", departureCity);
            query.setParameter("departureTime", departureTime);
            query.setParameter( "arrivalCity", arrivalCity);
            query.setParameter("arrivalTime", arrivalTime);
            query.executeUpdate();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
    }
}
