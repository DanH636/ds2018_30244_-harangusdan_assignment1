package BLL;

import DAO.UserDAO;
import Model.User;
import org.hibernate.cfg.Configuration;


import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/loginServlet")
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private UserDAO userDAO;

    public LoginServlet() {
        userDAO = new UserDAO(new Configuration().configure().buildSessionFactory());
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        resp.setContentType("text/html");

        // Post Parameters From The Request
        String user = req.getParameter("username");
        String pass = req.getParameter("password");

        User loggedUser = userDAO.findByUsername(user);
        HttpSession session = req.getSession();
        session.setAttribute("role", String.valueOf(loggedUser.getRole()));

        System.out.println("Username?= " + user + ", Password?= " + pass);

        // Print The Response
        PrintWriter out = resp.getWriter();
        out.write("<html><body><div id='serlvetResponse' style='text-align: center;'>");

        // Authentication Logic & Building The Html Response Code
        if ((user.equals(loggedUser.getUsername())) && (pass.equals(loggedUser.getPassword())) && loggedUser.getRole() == 1) {
            resp.sendRedirect("/admin.jsp");
        } else if ((user.equals(loggedUser.getUsername())) && (pass.equals(loggedUser.getPassword())) && loggedUser.getRole() == 0) {
            resp.sendRedirect("/client.jsp");
        } else {
            out.write("<p style='color: red; font-size: larger;'>You are not an authorised user! Please check with administrator!</p>");
        }
        out.write("</div></body></html>");
        out.close();
    }
}
