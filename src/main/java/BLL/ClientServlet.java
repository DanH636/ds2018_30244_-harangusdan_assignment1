package BLL;

import DAO.CityDAO;
import DAO.FlightDAO;
import Model.Flight;
import org.hibernate.HibernateException;
import org.hibernate.cfg.Configuration;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/clientServlet")
public class ClientServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private FlightDAO flightDAO;
    private CityDAO cityDAO;

    public ClientServlet() {
        flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
        cityDAO = new CityDAO((new Configuration().configure().buildSessionFactory()));
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        resp.setContentType("text/html");

        try {
            List<Flight> flights = flightDAO.getAllFlights();
            System.out.println(flights);
            PrintWriter out = resp.getWriter();
            out.write("<html><body><div id=\"serlvetResponse\" style=\"text-align: center;\">");
            out.write("<h2>All flights</h2>");
            out.write("<table><tr><th>ID</th><th>Flight number</th><th>Airplane Type</th><th>Departure city</th><th>Departure time</th><th>Arrival city</th><th>Arrival time</th></tr>");
            for(Flight flight: flights){
                out.write("<tr><td>" +flight.getId() + "</td><td>" + flight.getFlightNumber() + "</td><td>" + flight.getAirplaneType() + "</td><td>" + flight.getDepartureCity().getName() + "</td><td>" + flight.getDepartureTime() + "</td><td>" + flight.getArrivalCity().getName() + "</td><td>"+ flight.getArrivalTime() +"</td></tr>");
            }
            out.write("</table>");
            out.write("</div></body></html>");

        } catch (HibernateException e) {
            System.out.println(e);
        }
    }
}
