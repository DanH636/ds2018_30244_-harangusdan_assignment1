package BLL;

import DAO.CityDAO;
import DAO.FlightDAO;
import Model.Flight;
import org.hibernate.HibernateException;
import org.hibernate.cfg.Configuration;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.List;

@WebServlet("/earthToolsServlet")
public class EarthToolsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private FlightDAO flightDAO;
    private CityDAO cityDAO;

    public EarthToolsServlet() {
        flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
        cityDAO = new CityDAO((new Configuration().configure().buildSessionFactory()));
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        resp.setContentType("text/html");

        String getFlightNumber = req.getParameter("getFlightLocalTime");

        try {
            Flight flight = flightDAO.getFlight(Integer.parseInt(getFlightNumber));
            String urlString = String.format("http://new.earthtools.org/timezone/%f/%f", flight.getDepartureCity().getLatitude(), flight.getDepartureCity().getLongitude());
            URL url = new URL(urlString);

            SAXBuilder builder = new SAXBuilder();
            Document doc = builder.build(url);
            String departureTime = doc.getRootElement().getChild("localtime").getText();

            urlString = String.format("http://new.earthtools.org/timezone/%f/%f", flight.getArrivalCity().getLatitude(), flight.getArrivalCity().getLongitude());
            url = new URL(urlString);
            doc = builder.build(url);
            String arrivalTime = doc.getRootElement().getChild("localtime").getText();

            PrintWriter out = resp.getWriter();
            out.write("<html><body><div id=\"serlvetResponse\" style=\"text-align: center;\">");
            out.write("<p>Flight number: " + flight.getFlightNumber() + "</p>");
            out.write("<p> Departure city: " + flight.getDepartureCity().getName() + " with local time :" + departureTime + "</p>");
            out.write("<p> Arrival city: " + flight.getArrivalCity().getName() + " with local time :" + arrivalTime + "</p>");
            out.write("</div></body></html>");

        } catch (HibernateException e) {
            System.out.println(e);
        } catch (JDOMException e) {
            e.printStackTrace();
        }
    }
}
