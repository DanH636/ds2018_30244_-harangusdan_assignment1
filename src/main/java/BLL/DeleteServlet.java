package BLL;

import DAO.CityDAO;
import DAO.FlightDAO;
import Model.Flight;
import org.hibernate.HibernateException;
import org.hibernate.cfg.Configuration;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/deleteServlet")
public class DeleteServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private FlightDAO flightDAO;
    private CityDAO cityDAO;

    public DeleteServlet() {
        flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
        cityDAO = new CityDAO((new Configuration().configure().buildSessionFactory()));
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        resp.setContentType("text/html");

        String getFlightId = req.getParameter("deleteFlightId");

        try {
            flightDAO.deleteFlight(Integer.parseInt(getFlightId));
            PrintWriter out = resp.getWriter();
            out.write("<html><body><div id=\"serlvetResponse\" style=\"text-align: center;\">");
            out.write("<p>Successfully deleted flight</p>");
            out.write("</div></body></html>");
        } catch (HibernateException e) {
            System.out.println(e);
        }
    }
}
