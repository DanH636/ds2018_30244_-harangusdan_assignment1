package BLL;

import DAO.CityDAO;
import DAO.FlightDAO;
import Model.Flight;
import org.hibernate.HibernateException;
import org.hibernate.cfg.Configuration;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/adminServlet")
public class AdminServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private FlightDAO flightDAO;
    private CityDAO cityDAO;

    public AdminServlet() {
        flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
        cityDAO = new CityDAO((new Configuration().configure().buildSessionFactory()));
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        resp.setContentType("text/html");

        String flightNumber = req.getParameter("flightNumberAdd");
        String airplane = req.getParameter("airplaneTypeAdd");
        String depCity = req.getParameter("departureCityAdd");
        String depTime = req.getParameter("departureTimeAdd");
        String arrCity = req.getParameter("arrivalCityAdd");
        String arrTime = req.getParameter("arrivalTimeAdd");

        DateFormat format = new SimpleDateFormat("yyyy.MM.dd HH:mm");
        Date departTime = null;
        Date arrivTime = null;
        try {
            departTime = format.parse(depTime);
            arrivTime = format.parse(arrTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Flight flight = new Flight();
        flight.setFlightNumber(Integer.parseInt(flightNumber));
        flight.setAirplaneType(airplane);
        flight.setDepartureCity(cityDAO.findCity(depCity));
        flight.setDepartureTime(departTime);
        flight.setArrivalCity(cityDAO.findCity(arrCity));
        flight.setArrivalTime(arrivTime);

        try {
            flightDAO.addFlight(flight);

            PrintWriter out = resp.getWriter();
            out.write("<html><body><div id='serlvetResponse' style='text-align: center;'>");
            out.write("<h2>Successfully added flight</h2>");
            out.write("</div></body></html>");
        } catch (HibernateException e) {
            System.out.println(e);
        }
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        resp.setContentType("text/html");

        String getFlightNumber = req.getParameter("getFlightNumber");

        try {
            Flight flight = flightDAO.getFlight(Integer.parseInt(getFlightNumber));
            System.out.println(flight.toString());
            PrintWriter out = resp.getWriter();
            out.write("<html><body><div id=\"serlvetResponse\" style=\"text-align: center;\">");
            out.write("<p>Details for flight: " + flight.getId() + "</p>");
            out.write("<p>Flight number: " + flight.getFlightNumber() + "</p>");
            out.write("<p>Airplane type: " + flight.getAirplaneType() + "</p>");
            out.write("<p>Departure city: " + flight.getDepartureCity().getName() + "</p>");
            out.write("<p>Departure time: " + flight.getDepartureTime() + "</p>");
            out.write("<p>Arrival city: " + flight.getArrivalCity().getName() + "</p>");
            out.write("<p>Arrival time: " + flight.getArrivalTime() + "</p>");
            out.write("</div></body></html>");
        } catch (HibernateException e) {
            System.out.println(e);
        }
    }
}
