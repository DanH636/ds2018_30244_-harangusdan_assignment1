package BLL;

import DAO.CityDAO;
import DAO.FlightDAO;
import Model.Flight;
import org.hibernate.HibernateException;
import org.hibernate.cfg.Configuration;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/updateServlet")
public class UpdateServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private FlightDAO flightDAO;
    private CityDAO cityDAO;

    public UpdateServlet() {
        flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
        cityDAO = new CityDAO((new Configuration().configure().buildSessionFactory()));
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        resp.setContentType("text/html");

        String id = req.getParameter("idUpdate");
        String flightNumber = req.getParameter("flightNumberUpdate");
        String airplane = req.getParameter("airplaneTypeUpdate");
        String depCity = req.getParameter("departureCityUpdate");
        String depTime = req.getParameter("departureTimeUpdate");
        String arrCity = req.getParameter("arrivalCityUpdate");
        String arrTime = req.getParameter("arrivalTimeUpdate");

        DateFormat format = new SimpleDateFormat("yyyy.MM.dd HH:mm");
        Date departTime = null;
        Date arrivTime = null;
        try {
            departTime = format.parse(depTime);
            arrivTime = format.parse(arrTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Flight flight = new Flight();
        flight.setId(Integer.parseInt(id));
        flight.setFlightNumber(Integer.parseInt(flightNumber));
        flight.setAirplaneType(airplane);
        flight.setDepartureCity(cityDAO.findCity(depCity));
        flight.setDepartureTime(departTime);
        flight.setArrivalCity(cityDAO.findCity(arrCity));
        flight.setArrivalTime(arrivTime);

        try {
            flightDAO.updateFlight(flight.getId(),flight.getFlightNumber(),flight.getAirplaneType(),flight.getDepartureCity(),flight.getDepartureTime(),flight.getArrivalCity(),flight.getArrivalTime());

            PrintWriter out = resp.getWriter();
            out.write("<html><body><div id='serlvetResponse' style='text-align: center;'>");
            out.write("<h2>Successfully Updated flight</h2>");
            out.write("</div></body></html>");
        } catch (HibernateException e) {
            System.out.println(e);
        }
    }
}
