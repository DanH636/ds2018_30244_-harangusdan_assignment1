package BLL;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(urlPatterns = {"/*"})
public class AuthFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpSession session = req.getSession();
        String path = req.getRequestURI();
        System.out.println("The path is " + path);
        String[] parts = path.split("/");

        if (path == null || parts.length == 0) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }


        HttpServletResponse resp = (HttpServletResponse) servletResponse;
        if (parts[parts.length - 1].equals("admin.jsp") && (session.getAttribute("role") == null || session.getAttribute("role").equals("0"))) {
            resp.sendRedirect("client.jsp");
        }
        if (parts[parts.length - 1].equals("client.jsp") && (session.getAttribute("role") == null || session.getAttribute("role").equals("1"))) {
            resp.sendRedirect("admin.jsp");
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
